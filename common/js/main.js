//
//
//
$(document).ready(function(){$("a").bind("focus",function(){if(this.blur)this.blur();});});

// バーガーメニュー
$(document).ready(function() {
    $('.panel-btn').on('click', function() {
        $(this).toggleClass('active');
        return false;
    });
});
$(function(){
  $(".panel-btn").on("click", function() {
    $(".g-nav").toggle('fade', '', 800);
    if ($(".drawer-txt").text() == "MENU") {
          $(".drawer-txt").text("CLOSE");
    } else {
          $(".drawer-txt").text("MENU");
    }
  });
});

// スクロールでトップへ戻るボタンを表示
$(function() {
  $(window).scroll(function () {
    var scroll = $(window).scrollTop();
    var mainTop = $(".main-contents").offset().top;
    if(scroll > mainTop) {
      $(".totop").fadeIn('300');
    } else if(scroll < mainTop) {
      $(".totop").fadeOut('300');
    }
  });
});

//googolemapの高さ指定
$(document).ready( function(){
  var win = $(window).width();
  var wid = $("#map_canvas").width();
  var mapHeight = wid * 0.65;
  if (win <= 768) {
    $("#map_canvas").css("height", mapHeight);
  }
});
//googolemap設定
function initialize() {
  var latlng = new google.maps.LatLng(35.724314, 139.643475);
  var opts = {
    mapTypeControl: false,
    zoom: 16,
    center: latlng,
    scrollwheel: false,
    mapTypeId: google.maps.MapTypeId.ROADMAP
  };

  var map = new google.maps.Map(document.getElementById("map_canvas"), opts);
  // マーカーの設定
  var markerImg = {
     url: 'http://tohokuitfactory.com/___coding___/holisticure_brand/share/img/map_pin.png'  // マーカーの画像ファイルを指定
  };
  var marker = new google.maps.Marker({
    position: latlng,
    map: map,
    icon: markerImg
  });
  // 地図をグレースケールに
  var mapStyle = [{
    "stylers": [{ "lightness": -5 },{ "saturation": -100 }]
  }];
  var mapType = new google.maps.StyledMapType(mapStyle);
  map.mapTypes.set('GrayScaleMap', mapType);
  map.setMapTypeId('GrayScaleMap');
}
initialize();

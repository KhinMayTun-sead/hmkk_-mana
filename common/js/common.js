var portraitWidth, landscapeWidth;
    $(window).bind("resize", function() {
      if (Math.abs(window.orientation) === 0) {
        if (/Android/.test(window.navigator.userAgent)) {
          if (!portraitWidth)
            portraitWidth = $(window).width();
        } else {
          portraitWidth = $(window).width();
        }
        $("html").css("zoom", portraitWidth / 798);
      } else {
        if (/Android/.test(window.navigator.userAgent)) {
          if (!landscapeWidth)
            landscapeWidth = $(window).width();
        } else {
          landscapeWidth = $(window).width();
        }
        $("html").css("zoom", landscapeWidth / 798);
      }
    }).trigger("resize");

// menu  //

$(document).ready(function(){$("a").bind("focus",function(){if(this.blur)this.blur();});});
//ドロワーメニュー
$(document).ready(function() {
    $('.nav').on('click', function() {
        $(this).toggleClass('active');
        return false;
    });
});
$(function(){
  $(".nav").on("click", function() {
    $(".mnu-area").toggle();
    if ($(".").text() == "MENU") {
          $(".").text("CLOSE");
    } else {
          $(".").text("MENU");
    }
  });
});